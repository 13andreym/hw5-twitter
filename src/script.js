function displayPosts(posts, users) {
    const postsContainer = document.getElementById('posts-container');

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);

        const card = document.createElement('div');
        card.classList.add('card');

        const userInfo = document.createElement('div');
        userInfo.classList.add('user-info');
        userInfo.innerHTML = `<p>${user.name}</p><p>${user.email}</p>`;

        const postContent = document.createElement('div');
        postContent.classList.add('post-content');
        postContent.innerHTML = `<h2>${post.title}</h2><p>${post.body}</p>`;

        const deleteButton = document.createElement('button');
        deleteButton.classList.add('delete-button');
        deleteButton.textContent = 'Delete';

        deleteButton.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${post.id}`, {
                method: 'DELETE'
            })
                .then(response => {
                    if (response.ok) {
                        card.remove();
                    } else {
                        throw new Error('Failed to delete post');
                    }
                })
                .catch(error => console.error(error));
        });

        card.appendChild(userInfo);
        card.appendChild(postContent);
        card.appendChild(deleteButton);
        postsContainer.appendChild(card);
    });
}

function displayNewPost(post, user) {
    const postsContainer = document.getElementById('posts-container');

    const card = document.createElement('div');
    card.classList.add('card');

    const userInfo = document.createElement('div');
    userInfo.classList.add('user-info');
    userInfo.innerHTML = `<p>${user.name}</p><p>${user.email}</p>`;

    const postContent = document.createElement('div');
    postContent.classList.add('post-content');
    postContent.innerHTML = `<h2>${post.title}</h2><p>${post.body}</p>`;

    const deleteButton = document.createElement('button');
    deleteButton.classList.add('delete-button');
    deleteButton.textContent = 'Delete';

    deleteButton.addEventListener('click', () => {
        fetch(`https://ajax.test-danit.com/api/json/posts/${post.id}`, {
            method: 'DELETE'
        })
            .then(response => {
                if (response.ok) {
                    card.remove();
                } else {
                    throw new Error('Failed to delete post');
                }
            })
            .catch(error => console.error(error));
    });

    card.appendChild(userInfo);
    card.appendChild(postContent);
    card.appendChild(deleteButton);
    postsContainer.appendChild(card);
}


fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .then(users => {
        fetch('https://ajax.test-danit.com/api/json/posts')
            .then(response => response.json())
            .then(posts => {
                displayPosts(posts, users);
            })
            .catch(error => console.error('Error fetching posts:', error));
    })
    .catch(error => console.error('Error fetching users:', error));
